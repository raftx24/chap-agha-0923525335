<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    private array $users = [
        [
            'name' => 'admin',
            'email' => 'admin@chapagha.com',
            'password' => '123456',
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect($this->users)
            ->each(fn ($user) => User::create(['password' => bcrypt($user['password'])] + $user));
    }
}
