<?php

namespace App\Http\Controllers;

use App\Helper\Timer;
use App\Services\Fibonacci;
use App\Http\Requests\FibonacciRequest;

class FibController extends Controller
{
    public function index()
    {
        return view('fib');
    }

    public function calculate(FibonacciRequest $fibonacciRequest)
    {
        ini_set("xdebug.max_nesting_level", -1);

        $timer = new Timer();
        $fibonacci = new Fibonacci($fibonacciRequest->get('N'));
        $result = $timer->calc(fn() => $fibonacci->handle());

        return view('fib', [
            'N' => $fibonacciRequest->get('N'),
            'result' => $result,
            'time' => $timer->microSeconds()
        ]);
    }
}
