<?php

namespace App\Helper;

use Closure;

class Timer
{
    private float $start;
    private float $end;

    public function calc(Closure $closure)
    {
        $this->start = microtime(true);
        $result = $closure();
        $this->end = microtime(true);

        return $result;
    }

    public function miliSeconds(): int
    {
        return $this->duration() * 1000;
    }

    public function microSeconds(): int
    {
        return $this->duration() * 1000000;
    }

    protected function duration(): float
    {
        return $this->end - $this->start;
    }
}
