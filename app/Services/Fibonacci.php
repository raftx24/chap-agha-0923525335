<?php

namespace App\Services;

use Illuminate\Support\Facades\Cache;

class Fibonacci
{
    private array $memory;
    private int $n;
    private int $lastCache;

    public function __construct($n)
    {
        $this->n = $n;
        $this->memory = [1 => 1, 2 => 1];
        $this->lastCache = Cache::get('fibonacci_latest', 2);
    }

    public function handle()
    {
        if ($this->n > $this->lastCache) {
            $this->calc();
            $this->cache();
        }

        return $this->memory[$this->n]
            ?? Cache::get('fibonacci_' . $this->n);
    }

    private function calc()
    {
        $this->memory[$this->lastCache] ??= Cache::get('fibonacci_' . $this->lastCache);
        $this->memory[$this->lastCache - 1] ??= Cache::get('fibonacci_' . ($this->lastCache - 1));

        for ($i = $this->lastCache + 1; $i <= $this->n; $i++) {
            $this->memory[$i] = bcadd($this->memory[$i - 1], $this->memory[$i - 2]);
        }
    }

    private function cache()
    {
        $caches = [
            'fibonacci_latest' => $this->n
        ];

        for ($i = $this->lastCache + 1; $i <= $this->n; $i++) {
            $caches['fibonacci_' . $i] = $this->memory[$i];
        }

        Cache::putMany($caches);
    }
}
