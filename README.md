# ChapAgha

### Instruction
0. cp .env.example .env
1. docker-compose up -d
2. docker-compose exec app composer install
3. docker-compose exec app yarn install
4. docker-compose exec app yarn dev
5. docker-compose exec db mysql -u root -proot -e "CREATE DATABASE laravel"
6. docker-compose exec app php artisan key:generate
7. docker-compose exec app php migrate:fresh --seed

## open browser http://127.0.0.1:10080/
#### default user: admin@chapagha.com, password: 123456
### for email, you need to configure smtp first
