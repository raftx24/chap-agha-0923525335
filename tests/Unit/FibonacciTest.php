<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Helper\Timer;
use App\Services\Fibonacci;
use Illuminate\Support\Facades\Cache;

class FibonacciTest extends TestCase
{
    public function testLessThanTwo()
    {
        $this->assertEquals(1, (new Fibonacci(1))->handle());
        $this->assertEquals(1, (new Fibonacci(2))->handle());
    }

    public function testLessMoreThanTwo()
    {
        $this->assertEquals(2, (new Fibonacci(3))->handle());
        $this->assertEquals(3, (new Fibonacci(4))->handle());
        $this->assertEquals(5, (new Fibonacci(5))->handle());
    }

    public function testShouldCacheResult()
    {
        Cache::shouldReceive('get')
            ->once()
            ->with('fibonacci_latest', 2)
            ->andReturn(2);

        Cache::shouldReceive('putMany')
            ->once()
            ->with([
                'fibonacci_latest' => 5,
                'fibonacci_3' => 2,
                'fibonacci_4' => 3,
                'fibonacci_5' => 5,
            ]);

        (new Fibonacci(5))->handle();
    }

    public function testShouldNotReCache()
    {
        Cache::shouldReceive('get')
            ->once()
            ->with('fibonacci_latest', 2)
            ->andReturn(5);

        Cache::shouldReceive('get')
            ->once()
            ->with('fibonacci_5')
            ->andReturn(5);

        Cache::shouldNotReceive('put');

        $this->assertEquals(5, (new Fibonacci(5))->handle());
    }

    public function test500()
    {
        $timer = new Timer();
        $fibonacci = new Fibonacci(1000);
        $timer->calc(fn() => $fibonacci->handle());
        $this->assertEquals("43466557686937456435688527675040625802564660517371780402481729089536555417949051890403879840079255169295922593080322634775209689623239873322471161642996440906533187938298969649928516003704476137795166849228875", $fibonacci->handle());
        $this->assertTrue($timer->microSeconds() < 1000000);
    }
}
