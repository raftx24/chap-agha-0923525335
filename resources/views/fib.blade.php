@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Fibonacci Calculator</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                            <form method="post">
                                @csrf
                                <div class="form-group">
                                    <label for="inputN">N</label>
                                    <input type="number" name="N"
                                           class="form-control @error('N') is-invalid @enderror"
                                           id="inputN"
                                           aria-describedby="inputNHelp"
                                           value="{{$N ?? ''}}"
                                    >
                                    <small id="inputNHelp" class="form-text text-muted">Please enter N'th number in fibonacci series</small>
                                    @error('N')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <button type="submit" class="btn btn-primary">Calculate</button>
                                @isset($result)
                                    <div class="alert alert-success mt-2">
                                        <span>Result: <strong>{{ $result }}</strong> (<strong>{{ number_format($time) }}</strong> microseconds)</span>
                                    </div>
                                @endisset
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
